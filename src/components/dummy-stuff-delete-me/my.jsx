/* eslint-disable */
// @flow
import * as React from 'react';

type Props = {
  prop1: string,
}
type State = {
  foo: string,
}


class My extends React.Component<Props, State> {

  componentWillMount(){
    this.setState({ "foo": "bar" })
  }

  render() {
    return (<h4>this is my message</h4>);
  }
}

export { My };
export default My;
