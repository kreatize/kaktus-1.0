/* eslint-disable */
import React, { Component } from 'react';
import { with$Http } from '../../services/$http/$http';
import My from "./my";

class Comp extends Component {
  state = {
    hasError: false,
  };

  componentWillMount() {
    // setTimeout(() => this.setState({ hasError: true }), 3 * 1000);
  }

  render() {
    if (this.state.hasError) throw new Error('error');
    return (
      <React.Fragment>
        <h3>I am dummy! {this.state.hasError ? 'Y' : 'N'}</h3>
        <p>{this.props.$http}</p>
      </React.Fragment>
    );
  }
}

const DummyComponent = with$Http(Comp)

export { DummyComponent };
export default DummyComponent;
