import React, { Component } from 'react';
import { compose } from 'recompose';

import Layout__4xx from '../../../../layouts/4xx/layout__4xx';
import { $history } from '../../../../services/$history/$history';
import { with$ErrorReporter } from '../../../../services/$error-reporter/$error-reporter';

class Comp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      hasError: false,
    };
  }

  componentDidCatch(error, info) {
    this.setState(state => ({ ...state, hasError: true }));
    this.props.$errorReporter.reportError(error, info);
    $history.push('/error/4xx');
  }

  render() {
    if (this.state.hasError) {
      return (
        <Layout__4xx>
          <div>Sorry, something went wrong.</div>
        </Layout__4xx>
      );
    }
    return this.props.children;
  }
}

const decorate = compose(with$ErrorReporter);

const ErrorBoundaryGeneric = decorate(Comp);

export { ErrorBoundaryGeneric };
export default ErrorBoundaryGeneric;
