/* eslint-disable react/no-array-index-key */
import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import { allRoutes } from '../routes/index';

const Routes = () => (
  <div className="routes">
    <Switch>
      {allRoutes.map(({ exact, path, component }, i) => (
        <Route exact={exact} path={path} component={component} key={i} />
      ))}
      <Redirect to="/front/login" />
    </Switch>
  </div>
);

export default Routes;
