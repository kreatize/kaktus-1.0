import React from 'react';
import { I18nextProvider } from 'react-i18next';

import Routes from './routes';
import { ErrorBoundaryGeneric } from './error/boundary/generic/error-boundary-generic';
import { $i18n } from '../services/$i18n/$i18n';
import { $history, Router } from '../services/$history/$history';
import '../../vendor/material-dashboard-pro-react-v1.2.0/src/assets/scss/material-dashboard-pro-react.scss?v=1.2.0';

export const App = () => (
  <ErrorBoundaryGeneric>
    <Router history={$history}>
      <I18nextProvider i18n={$i18n}>
        <Routes />
      </I18nextProvider>
    </Router>
  </ErrorBoundaryGeneric>
);

export default App;
