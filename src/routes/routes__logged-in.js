import BundleSupplierSettings from '../pages/app/bundle-supplier/settings/bundle-supplier-settings';
import BundleSupplierListing from '../pages/app/bundle-supplier/listing/bundle-supplier-listing';

export const routes__loggedIn = [
  {
    path: '/app/bundle-suppliers',
    component: BundleSupplierListing,
    exact: true,
  },
  {
    path: '/app/bundle-suppliers/:bundleSupplierId',
    component: BundleSupplierSettings,
    exact: true,
  },
];
export default routes__loggedIn;
