import Login from '../pages/front/login/login';

export const routes__loggedOut = [
  {
    path: '/front/login',
    component: Login,
    exact: true,
  },
];
export default routes__loggedOut;
