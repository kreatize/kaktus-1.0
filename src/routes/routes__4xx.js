import Error4xx from '../pages/error/4xx/error-4xx';

export const routes__4xx = [
  {
    path: '/error/4xx',
    component: Error4xx,
    exact: true,
  },
];
export default routes__4xx;
