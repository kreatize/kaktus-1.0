import { routes__4xx } from './routes__4xx';
import { routes__loggedIn } from './routes__logged-in';
import { routes__loggedOut } from './routes__logged-out';

export const allRoutes = [...routes__4xx, ...routes__loggedIn, ...routes__loggedOut];
export default allRoutes;
