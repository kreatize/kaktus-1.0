import React from 'react';

export const Layout__LoggedOut = ({ children }) => (
  <div>
    {children}
  </div>
);

export default Layout__LoggedOut;
