import React from 'react';

export const Layout__4xx = ({ children }) => (
  <div>
    <h3>Error!</h3>
    {children}
  </div>
);

export default Layout__4xx;
