import React from 'react';
import Dashboard from './theme__logged-in';
import { with$Auth } from '../../services/$auth/$auth';

export const Layout__LoggedIn = props => (
  <Dashboard {...props}>
    {props.children}
  </Dashboard>
);

export default with$Auth(Layout__LoggedIn);
