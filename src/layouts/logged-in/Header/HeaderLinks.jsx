/* eslint-disable */
import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import { Manager, Target, Popper } from 'react-popper';

// @material-ui/core components
import withStyles from '@material-ui/core/styles/withStyles';
import MenuItem from '@material-ui/core/MenuItem';
import MenuList from '@material-ui/core/MenuList';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Paper from '@material-ui/core/Paper';
import Grow from '@material-ui/core/Grow';
import Hidden from '@material-ui/core/Hidden';

// @material-ui/icons
import Person from '@material-ui/icons/Person';
import Notifications from '@material-ui/icons/Notifications';
import Dashboard from '@material-ui/icons/Dashboard';
import Search from '@material-ui/icons/Search';

// core components
import CustomInput from 'components/CustomInput/CustomInput.jsx';
import Button from 'components/CustomButtons/Button.jsx';

import headerLinksStyle from 'assets/jss/material-dashboard-pro-react/components/headerLinksStyle';

import { compose } from 'recompose';

import { withT } from '../../../services/$i18n/$i18n';
import { with$Auth } from '../../../services/$auth/$auth';


class HeaderLinks extends React.Component {
  state = {
    open: false,
  };
  handleClick = () => {
    this.setState({ open: !this.state.open });
  };
  handleClose = () => {
    this.setState({ open: false });
  };
  render() {
    const {
      classes, rtlActive, t, $auth: { doLogout },
    } = this.props;
    const { open } = this.state;
    const searchButton =
      `${classes.top
      } ${
        classes.searchButton
      } ${
        classNames({
          [classes.searchRTL]: rtlActive,
        })}`;
    const dropdownItem =
      `${classes.dropdownItem
      } ${
        classNames({
          [classes.dropdownItemRTL]: rtlActive,
        })}`;
    const wrapper = classNames({
      [classes.wrapperRTL]: rtlActive,
    });
    const managerClasses = classNames({
      [classes.managerClasses]: true,
    });
    return (
      <div className={wrapper}>

        <Manager className={managerClasses}>
          <Target>
            <Button
              color="transparent"
              justIcon
              aria-label="Notifications"
              aria-owns={open ? 'menu-list' : null}
              aria-haspopup="true"
              onClick={this.handleClick}
              className={rtlActive ? classes.buttonLinkRTL : classes.buttonLink}
              muiClasses={{
                label: rtlActive ? classes.labelRTL : '',
              }}
            >
              <Person
                className={
                  `${classes.headerLinksSvg
                  } ${
                  rtlActive
                    ? `${classes.links} ${classes.linksRTL}`
                    : classes.links}`
                }
              />
              <span className={classes.notifications}>5</span>
              <Hidden mdUp>
                <span onClick={this.handleClick} className={classes.linkText}>
                  {rtlActive ? 'إعلام' : 'Notification'}
                </span>
              </Hidden>
            </Button>
          </Target>
          <Popper
            placement="bottom-start"
            eventsEnabled={open}
            className={
              `${classNames({ [classes.popperClose]: !open })
              } ${
              classes.pooperResponsive}`
            }
          >
            <ClickAwayListener onClickAway={this.handleClose}>
              <Grow
                in={open}
                id="menu-list"
                style={{ transformOrigin: '0 0 0' }}
              >
                <Paper className={classes.dropdown}>
                  <MenuList role="menu">
                    <MenuItem
                      onClick={doLogout}
                      className={dropdownItem}
                    >
                      {t('common__logout')}
                    </MenuItem>
                  </MenuList>
                </Paper>
              </Grow>
            </ClickAwayListener>
          </Popper>
        </Manager>

      </div>
    );
  }
}

HeaderLinks.propTypes = {
  classes: PropTypes.object.isRequired,
  rtlActive: PropTypes.bool,
};

const enhance = compose(
  withT('default'),
  withStyles(headerLinksStyle),
  with$Auth,
);

export default enhance(HeaderLinks);
