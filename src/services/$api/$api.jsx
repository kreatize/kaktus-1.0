import React from 'react';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/dom/ajax';
import { some, includes } from 'lodash';
import { $http } from '../$http/$http';

export const $api = {
  login$: (email, password) =>
    Observable.ajax({
      url: `${APP_RFQ_API_URL}/auth/login`,
      body: {
        email, password,
      },
      method: 'POST',
    })
      .do((resp) => {
        if (!(resp.response.asset_id && resp.response.asset_id.length > 0)) {
          throw new Error('No asset_id in login result');
        }
      }),

  userMe$: ({ asset_id, token }) =>
    Observable.fromPromise($http(`${APP_RFQ_API_URL}/auth/user`, {
      headers: {
        asset_id,
        Authorization: token,
      },
    }))
      .do(({ data: { data: { role, scopes } } }) => {
        if (!some(scopes, s => includes(['s-dashboard'], s.name)) &&
          role !== 'admin') {
          throw new Error('The authenticated user does not have a s-dashboard or admin scope');
        }
      }),
};

export const $ApiContext = React.createContext($api);

export function with$Api(Component) {
  return function ComponentWith$Api(props) {
    return (
      <$ApiContext.Consumer>
        {_$api => <Component {...props} $api={_$api} />}
      </$ApiContext.Consumer>
    );
  };
}

