import React from 'react';
import axios from 'axios';
import _ from 'lodash';

import { $history } from '../$history/$history';

export const $http = axios.create();
$http.interceptors.response.use(
  resp => resp,
  (err) => {
    if (_.includes([401, 403], err.response.status)) {
      $history.push('/front/login');
    } else {
      $history.push('/error/4xx');
    }
    // TODO: do this?
    // throw err
  },
);

export const $HttpContext = React.createContext($http);

export function with$Http(Component) {
  return function ComponentWith$Http(props) {
    return (
      <$HttpContext.Consumer>
        {_$http => <Component {...props} $http={_$http} /> }
      </$HttpContext.Consumer>
    );
  };
}

