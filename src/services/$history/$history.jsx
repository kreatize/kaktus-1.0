import React from 'react';

import { Router } from 'react-router-dom';
import { createBrowserHistory } from 'history';

const $history = createBrowserHistory();
export const $HistoryContext = React.createContext($history);

export {
  $history,
  Router,
};

export function with$History(Component) {
  return function ComponentWith$History(props) {
    return (
      <$HistoryContext.Consumer>
        {_$history => <Component {...props} $history={_$history} /> }
      </$HistoryContext.Consumer>
    );
  };
}

