/* eslint-disable no-console */
import React from 'react';

function reportError(error, info) {
  console.log("I am supposed to submit the below error to AWS via Khapi, pending implementation I'll just show it to you below :-)");
  console.log(error);
  console.log(info);
}

export const $errorReporter = {
  reportError,
};

export const $ErrorReporterContext = React.createContext($errorReporter);

export function with$ErrorReporter(Component) {
  return function ComponentWith$ErrorReporter(props) {
    return (
      <$ErrorReporterContext.Consumer>
        {_$errorReporter => <Component {...props} $errorReporter={_$errorReporter} /> }
      </$ErrorReporterContext.Consumer>
    );
  };
}

