import i18n from 'i18next';
import { reactI18nextModule, translate } from 'react-i18next';

i18n
  .use(reactI18nextModule) // if not using I18nextProvider
  .init({
    fallbackLng: 'de',
    debug: true,
    interpolation: {
      escapeValue: false, // not needed for react!!
    },
    // react i18next special options (optional)
    react: {
      wait: false,
      bindI18n: 'languageChanged loaded',
      bindStore: 'added removed',
      nsMode: 'default',
    },

    resources: {
      de: {
        default: {
          login__title: 'Wilkommen im ihrem Digitalen Fertigungsanfrage Cockpit',
          login__subtitle: 'Bitte einloggen.',
          login__emailLabel: 'E-Mail',
          login__passwordLabel: 'Passwort',

          common__yalla: 'Anmelden',
          common__logout: 'Ausloggen',
        },
      },
    },
  });

export const $i18n = i18n;
export const withT = translate;
export default $i18n;
