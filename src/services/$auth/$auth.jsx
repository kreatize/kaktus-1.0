import React from 'react';
import { $history } from '../$history/$history';

export const $auth = {
  getAuthInfo: () => {
    try {
      return (typeof window !== 'undefined' &&
        window.localStorage &&
        window.localStorage.authInfo &&
        JSON.parse(window.localStorage.authInfo)) ||
        null;
    } catch (err) {
      return null;
    }
  },
  getUserInfo: () => {
    try {
      return (typeof window !== 'undefined' &&
        window.localStorage &&
        window.localStorage.userInfo &&
        JSON.parse(window.localStorage.userInfo)) ||
        null;
    } catch (err) {
      return null;
    }
  },
  doLogout: (e) => {
    e.preventDefault();
    window.localStorage.removeItem('authInfo');
    window.localStorage.removeItem('userInfo');
    $history.push('/front/login');
    return false;
  },
};

export const $AuthContext = React.createContext($auth);

export function with$Auth(Component) {
  return function ComponentWith$Auth(props) {
    return (
      <$AuthContext.Consumer>
        {_$auth => <Component {...props} $auth={_$auth} /> }
      </$AuthContext.Consumer>
    );
  };
}
