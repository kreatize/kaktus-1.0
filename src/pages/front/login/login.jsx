/* eslint-disable no-alert, class-methods-use-this, react/no-unused-state */
// @flow
import React, { Component } from 'react';
import { compose } from 'recompose';
import Observable from 'rxjs';

import { with$Api } from '../../../services/$api/$api';
import Layout__LoggedOut from '../../../layouts/logged-out/layout__logged-out';
import LoginPage from './theme__login-page';

type Props = {
  $api: {
    login$: (username: ?string, password: ?string) => Observable,
    userMe$: {} => Observable,
  },
  history: { push: string => void },
}

type State = {
  isLoggingIn: boolean,
  emailOrPasswordInvalidError: boolean,
  loginError: boolean,
  password: ?string,
  email: ?string,
}

class Comp extends Component <Props, State> {
  onEmailChange;
  onPasswordChange;
  doLogin;

  constructor() {
    super();
    this.onEmailChange = this._onEmailChange.bind(this);
    this.onPasswordChange = this._onPasswordChange.bind(this);
    this.doLogin = this._doLogin.bind(this);
  }

  state = {
    isLoggingIn: false,
    emailOrPasswordInvalidError: false,
    loginError: false,
    email: null,
    password: null,
  };

  _onEmailChange(e) {
    const email = e.target.value;
    this.setState({ email });
  }

  _onPasswordChange(e) {
    const password = e.target.value;
    this.setState({ password });
  }

  _doLogin(e) {
    e.preventDefault();

    const { login$, userMe$ } = this.props.$api;
    const { email, password } = this.state;
    this.setState({ isLoggingIn: true, loginError: false, emailOrPasswordInvalidError: false });

    login$(email, password)
      .map(res => ({ loginResult: res }))
      .switchMap(
        d => userMe$(d.loginResult.response),
        (d, res) => Object.assign(d, { userMeResult: res.data.data }),
      )
      .subscribe(
        d => this.doLoginResultHandle.bind(this)(d),
        d => this.doHandleError.bind(this)(d),
      );
  }

  doLoginResultHandle({ loginResult, userMeResult }) {
    if (loginResult.status === 200) {
      window.localStorage.authInfo = JSON.stringify(loginResult.response);
      window.localStorage.userInfo = JSON.stringify(userMeResult);

      this.props.history.push('/app/bundle-suppliers');
    }
  }

  doHandleError(error) {
    alert(`Please try again ${error}`); // eslint-disable no-alert
  }

  render() {
    return (
      <Layout__LoggedOut>
        <LoginPage
          onEmailChange={this.onEmailChange}
          onPasswordChange={this.onPasswordChange}
          onSubmit={this.doLogin}
        />
      </Layout__LoggedOut>
    );
  }
}

const decorate = compose(with$Api);

export const Login = decorate(Comp);
export default Login;
