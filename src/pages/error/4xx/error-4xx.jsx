import React from 'react';
import Layout__4xx from '../../../layouts/4xx/layout__4xx';

export const Error4xx = () => (
  <Layout__4xx>
    <div>oops!</div>
  </Layout__4xx>
);

export default Error4xx;
