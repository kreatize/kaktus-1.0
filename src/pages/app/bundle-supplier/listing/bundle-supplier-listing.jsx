/* eslint-disable arrow-parens */
import React from 'react';
import { Link } from 'react-router-dom';

import Layout__LoggedIn from '../../../../layouts/logged-in/layout__logged-in';

export const BundleSupplierListing = (props) => (
  <Layout__LoggedIn {...props}>
    <h3>Bundle supplier listing</h3>
    <Link to="/app/bundle-suppliers/42">Bundle supplier #42!</Link>
  </Layout__LoggedIn>
);

export default BundleSupplierListing;
