/* eslint-disable arrow-parens */
import React from 'react';
import Layout__LoggedIn from '../../../../layouts/logged-in/layout__logged-in';

export const BundleSupplierSettings = (props) => (
  <Layout__LoggedIn {...props}>
    <div>Bundle supplier settings: {props.match.params.bundleSupplierId}</div>
  </Layout__LoggedIn>
);

export default BundleSupplierSettings;
