import React from 'react';
import Layout__LoggedIn from '../../../../layouts/logged-in/layout__logged-in';

export const Logout = () => (
  <Layout__LoggedIn>
    <div>Logout</div>
  </Layout__LoggedIn>
);

export default Logout;
