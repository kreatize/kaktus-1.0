/* eslint-disable react/jsx-filename-extension, import/no-extraneous-dependencies */

import React from 'react';
import { render } from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import { App } from './components/app';

render(
  <AppContainer>
    <App />
  </AppContainer>,
  document.getElementById('app'),
);
